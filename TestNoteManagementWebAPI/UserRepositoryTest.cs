using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using NoteManagementWebAPI.Controllers;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Repository;

namespace TestNoteManagementWebAPI;

public class UserRepositoryTest
{
    private List<User> users;
    private Mock<IMapper> _mockMapper;
    DbContextOptions options = new DbContextOptionsBuilder<NoteManagementDbContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;
    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        //var options = new DbContextOptionsBuilder<NoteManagementDbContext>()
        //    .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
        //    .Options;
        
        users = new List<User>()
        {
            new User(){UserId=1, UserName="username1", Password="123", Email="abc@gmail.com", FullName="fullname"},
            new User(){UserId=2, UserName="username2", Password="123", Email="abc@gmail.com", FullName="fullname"},
            new User(){UserId=3, UserName="username3", Password="123", Email="abc@gmail.com", FullName="fullname"},
        };
        
        using (var context = new NoteManagementDbContext(options))
        {
            context.Users.AddRange(users);
            context.SaveChanges();
        };


        _mockMapper = new Mock<IMapper>();
    }
    [SetUp]
    public void Setup()
    {

    }

    [Test]
    public void AssignAdmin_ThrowExceptions_WhenUserIdOutOfRange()
    {
        // arrange                        
        var dbcontext = new NoteManagementDbContext(options);
        var userRepo = new UserRepository(dbcontext, _mockMapper.Object);

        // act

        // assert
        Assert.Throws<Exception>(() => userRepo.AssignAdmin(4));
    }
}