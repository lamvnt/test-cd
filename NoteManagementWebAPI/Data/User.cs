﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NoteManagementWebAPI.Data;

public class User
{
    [Key]
    public int UserId { get; set; }
    [Required]
    [MaxLength(50)]
    public string UserName { get; set; } = null!;
    [Required]
    [MaxLength(50)]
    public string Password { get; set; } = null!;
    [Required]
    public string FullName { get; set; } = null!;
    [Required]
    [EmailAddress]
    public string Email { get; set; } = null!;
    public string? AvatarUrl { get; set; }
    public DateTime CreateAt { get; set; }
    public DateTime LastVisited { get; set; }
    public bool IsAdmin { get; set; }
    public bool IsLocked { get; set; }
}
