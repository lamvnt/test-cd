﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NoteManagementWebAPI.Data;

public class Note
{
    [Key]
    public Guid NoteId { get; set; }
    [MaxLength(50)]
    public string Title { get; set; } = null!;
    [Required]
    public string Content { get; set; } = null!;
    public DateTime CreateAt { get; set; }
    public DateTime ModifiedAt { get; set; }

    // relationship user-model: 1-N
    public int UserId { get; set; }
    [ForeignKey("UserId")]
    public User User { get; set; } = null!;

}
