﻿using Microsoft.EntityFrameworkCore;

namespace NoteManagementWebAPI.Data;

public class NoteManagementDbContext : DbContext
{
    public NoteManagementDbContext(DbContextOptions options) : base(options)
    {
    }
    #region DbSet
    public virtual DbSet<User> Users { get; set; }
    public DbSet<Note> Notes { get; set; }
    #endregion
    // fluent api
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("Users");
            entity.Property(e => e.UserId)
                    .UseIdentityColumn(1, 1);
            entity.HasIndex(e => e.UserName).IsUnique();
            entity.HasIndex(e => e.Email).IsUnique();
            entity.Property(e => e.CreateAt)
                    .HasDefaultValue(DateTime.Now);
            entity.Property(e => e.LastVisited)
                    .HasDefaultValue(DateTime.Now);
            entity.Property(e => e.IsLocked)
                    .HasDefaultValue(false);
            entity.Property(e => e.IsAdmin)
                    .HasDefaultValue(false);
        });

        modelBuilder.Entity<Note>(entity =>
        {
            entity.ToTable("Notes");
            entity.Property(e => e.Title)
                    .HasDefaultValue("Untitle");
            entity.Property(e => e.CreateAt)
                    .HasDefaultValue(DateTime.Now);
            entity.Property(e => e.ModifiedAt)
                    .HasDefaultValue(DateTime.Now);
        });
    }
}
