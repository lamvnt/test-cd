﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Model;
using NoteManagementWebAPI.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace NoteManagementWebAPI.Repository;

public class AccountsRepository : IAccountsRepository
{
    private readonly NoteManagementDbContext _context;
    private readonly IConfiguration _configuration;
    private readonly ILogger<AccountsRepository> _logger;

    public AccountsRepository(NoteManagementDbContext context, IConfiguration configuration,
        ILogger<AccountsRepository> logger)
    {
        _context = context;
        _configuration = configuration;
        _logger = logger;
    }

    public ApiResponse Login(LoginModel model)
    {
        var user = _context.Users.SingleOrDefault(u => u.UserName == model.UserName
                                        && u.Password == model.Password);
        // đăng nhập thất bại
        if (user == null)
        {
            return new ApiResponse
            {
                Success= false,
                Message = "Invalid username/password.",
            };
        }
        // đăng nhập thành công
        // ghi log lịch sử đăng nhập
        user.LastVisited= DateTime.UtcNow;
        _context.Users.Update(user);
        _context.SaveChanges();
        _logger.LogInformation($"Login history: @{user.UserName} has logged in.");

        // trả về token
        return new ApiResponse
        {
            Success = true,
            Message = "Login successfully.",
            Data = GenerateToken(user)
        };
    }

    public ApiResponse Register(RegisterModel model)
    {
        var user = _context.Users.SingleOrDefault(u => u.UserName == model.UserName
                                                            || u.Email == model.Email);        
        // đăng kí thất bại
        if (user != null)
        {
            return new ApiResponse
            {
                Success = false,
                Message = "Username or email has been used."
            };
        }
        // đăng kí thành công, phải đăng nhập mới được cấp token
        var newUser = new User
        {
            UserName = model.UserName,
            Password = model.Password,
            FullName = model.FullName,
            Email = model.Email            
        };
        _context.Users.Add(newUser);
        _context.SaveChanges();

        return new ApiResponse
        {
            Success = true,
            Message = "Account has been registered successfully. Please login to access the server."
        };
    }

    public void UpdateAvatarUrl(int userId, string avatarUrl)
    {
        var user = _context.Users.SingleOrDefault(u => u.UserId == userId);
        user!.AvatarUrl = avatarUrl;
        _context.Users.Update(user);
        _context.SaveChanges();
    }

    // Viết method cấp token
    private string GenerateToken(User user)
    {
        string userRole;
        if (user.IsAdmin)
        {
            userRole = "Admin";
        } else
        {
            userRole = "User";
            if (!user.IsLocked)
            {
                userRole = "ActiveUser";
            }
        }
        // cơ chế để sinh token
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var serectKeyBytes = Encoding.UTF8.GetBytes(_configuration["JWT:SecretKey"]!);
        // Generate token
        var tokenDescription = new SecurityTokenDescriptor
        {
            // định nghĩa các đặc trưng của người dùng (claimsIdentity)
            // sau khi đăng nhập thành công thì client sẽ lưu lại & gửi kèm theo mỗi request đến server
            // server muốn lấy chỉ cần
            // var identity = HttpContext.User.Identity as ClaimsIdentity;
            // IList < Claim > claims = identity.Claims.ToList(); // trả ra danh sách các Claims, truy cập nó như truy cập mảng, vd: claims[0]
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("UserId", user.UserId.ToString()),
                new Claim("UserName", user.UserName),
                // roles
                new Claim(ClaimTypes.Role, userRole),
                new Claim(ClaimTypes.Name, user.FullName),
                new Claim(ClaimTypes.Email, user.Email),                
                    
                new Claim("TokenId", Guid.NewGuid().ToString())
                }),
            // Expired time of token
            Expires = DateTime.UtcNow.AddMinutes(30), // thoi han token: 1 phut
            SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(serectKeyBytes), SecurityAlgorithms.HmacSha256)

        };
        var token = jwtTokenHandler.CreateToken(tokenDescription);
        return jwtTokenHandler.WriteToken(token); //trả chuỗi token
    }
}
