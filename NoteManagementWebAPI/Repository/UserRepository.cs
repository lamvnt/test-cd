﻿using AutoMapper;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Model;

namespace NoteManagementWebAPI.Repository;

public class UserRepository : IUserRepository
{
    private readonly NoteManagementDbContext _context;
    private readonly IMapper _mapper;

    public UserRepository(NoteManagementDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public void AssignAdmin(int userId)
    {
        try
        {
            var user = _context.Users.SingleOrDefault(u => u.UserId == userId);
            user!.IsAdmin = true;
            _context.Update(user);
            _context.SaveChanges();
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    public void DisableUser(int userId)
    {
        var user = _context.Users.SingleOrDefault(u => u.UserId == userId);
        user!.IsLocked = true;
        _context.Update(user);
        _context.SaveChanges();
    }

    public IEnumerable<UserModel> GetAll()
    {
        var users = _context.Users;
        var model = _mapper.Map<List<UserModel>>(users);
        foreach (var m in model)
        {
            m.TotalNote = _context.Notes.Where(n => n.UserId == m.UserId).Count();
        }
        return model;
    }

}
