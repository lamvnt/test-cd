﻿using NoteManagementWebAPI.Model;

namespace NoteManagementWebAPI.Repository;

public interface INotesRepository
{
    public Guid Create(NoteModel model, int userId);
    public IEnumerable<NoteVM> GetAll();
    public IEnumerable<NoteVM> GetNotesByUser(int userId);
    public NoteVM GetById(Guid id);
    public void Update(Guid id, NoteModel model);
    public void Delete(Guid id);

    public IEnumerable<NoteVM> GetTopCurrentNote(int number);
}
