﻿using NoteManagementWebAPI.Model;

namespace NoteManagementWebAPI.Repository;

public interface IUserRepository
{
    public IEnumerable<UserModel> GetAll();
    public void AssignAdmin(int userId);
    public void DisableUser(int userId);
}
