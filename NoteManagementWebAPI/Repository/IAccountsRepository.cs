﻿using NoteManagementWebAPI.Model;
using NoteManagementWebAPI.Models;

namespace NoteManagementWebAPI.Repository;

public interface IAccountsRepository
{
    public ApiResponse Login(LoginModel model);
    public ApiResponse Register(RegisterModel model);

    public void UpdateAvatarUrl(int userId, string avatarUrl);

}
