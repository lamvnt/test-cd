﻿using AutoMapper;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Helper;
using NoteManagementWebAPI.Model;
using System.Security.Claims;
using static Azure.Core.HttpHeader;

namespace NoteManagementWebAPI.Repository;

public class NotesRepository : INotesRepository
{
    private readonly NoteManagementDbContext _context;
    private readonly IMapper _mapper;

    public NotesRepository(NoteManagementDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public Guid Create(NoteModel model, int userId)
    {
        try
        {
            var note = _mapper.Map<Note>(model);
            note.NoteId = Guid.NewGuid();
            note.UserId = userId;
            _context.Notes.Add(note);
            _context.SaveChanges();
            return note.NoteId;
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
        
    }

    public void Delete(Guid id)
    {
        var note = _context.Notes.SingleOrDefault(n => n.NoteId == id);
        if (note != null)
        {
            _context.Notes.Remove(note);
            _context.SaveChanges(true);

        }
        else
        {
            throw new Exception("Note of this Id does not exist.");
        }
    }

    public IEnumerable<NoteVM> GetAll()
    {
        var noteList = _context.Notes.ToList();
        return _mapper.Map<List<NoteVM>>(noteList);
    }

    public NoteVM GetById(Guid id)
    {
        var note = _context.Notes.SingleOrDefault(n => n.NoteId == id);
        return _mapper.Map<NoteVM>(note);
    }

    public IEnumerable<NoteVM> GetNotesByUser(int userId)
    {
        var notes = _context.Notes.Where(n => n.UserId == userId);
        return _mapper.Map<List<NoteVM>>(notes);
    }

    public IEnumerable<NoteVM> GetTopCurrentNote(int number)
    {
        try
        {
            List<Note> notes = _context.Notes.ToList();
            notes.Sort(new NoteComparer()); // sort by create Date
            var topCurrentNotes = notes.Take(number);
            var noteModels = _mapper.Map<List<NoteVM>>(topCurrentNotes);
            foreach (var m in noteModels)
            {
                m.UserName = _context.Users.SingleOrDefault(u => u.UserId == m.UserId)!.UserName;
            }
            return noteModels;
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    public void Update(Guid id, NoteModel model)
    {
        var note = _context.Notes.SingleOrDefault(n => n.NoteId == id);
        if (note != null)
        {
            var updateNote = _mapper.Map<Note>(model);
            note.Title = updateNote.Title;
            note.Content = updateNote.Content;
            note.ModifiedAt = DateTime.UtcNow;
            _context.Notes.Update(note);
            _context.SaveChanges(true);

        } else
        {
            throw new Exception("Note of this Id does not exist.");
        }
    }
    private bool NoteExists(Guid id)
    {
        return _context.Notes.Any(e => e.NoteId == id);
    }
}
