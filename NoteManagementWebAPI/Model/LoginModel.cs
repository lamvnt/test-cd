﻿using System.ComponentModel.DataAnnotations;

namespace NoteManagementWebAPI.Model;

public class LoginModel
{
    [Required]
    [MaxLength(50)]
    public string UserName { get; set; } = null!;
    [Required]
    [MaxLength(50)]
    public string Password { get; set; } = null!;
}
