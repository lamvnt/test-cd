﻿namespace NoteManagementWebAPI.Model;

public class MailModel
{
    public string Subject { get; set; }
    public string Body { get; set; }
}
