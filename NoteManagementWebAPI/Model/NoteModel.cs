﻿using NoteManagementWebAPI.Data;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NoteManagementWebAPI.Model;

public class NoteModel // for input
{
    [MaxLength(50)]
    public string Title { get; set; } = null!;
    [Required]
    public string Content { get; set; } = null!;
}
public class NoteVM : NoteModel // for view
{
    [Key]
    public Guid NoteId { get; set; }
    public DateTime CreateAt { get; set; }
    public DateTime ModifiedAt { get; set; }
    public int UserId { get; set; }
    public string UserName { get; set; }
}
