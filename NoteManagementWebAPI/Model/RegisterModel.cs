﻿using System.ComponentModel.DataAnnotations;

namespace NoteManagementWebAPI.Model;

public class RegisterModel
{
    [Required]
    public string UserName { get; set; } = null!;
    [Required]
    [MaxLength(50)]
    public string Password { get; set; } = null!;
    [Required]
    [MaxLength(50)]
    public string PasswordConfirm { get; set; } = null!;
    [Required]
    public string FullName { get; set; } = null!;
    [Required]
    [EmailAddress]
    public string Email { get; set; } = null!;
}
