﻿using NoteManagementWebAPI.Data;

namespace NoteManagementWebAPI.Helper;

public class NoteComparer : IComparer<Note>
{
    public int Compare(Note? x, Note? y)
    {
        return x.CreateAt.CompareTo(y.CreateAt);
    }
}
