﻿using System.Net.Mail;
using System.Net;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace NoteManagementWebAPI.Helper;

public class MailUtils
{
    private readonly IConfiguration _config;

    public MailUtils(IConfiguration config)
    {
        _config = config;
    }

    // Gửi mail bằng server của Google (Gmail)
    public async Task<bool> SendGMailAsync(string email, string subject, string body)
    {
        try
        {
            var _email = _config["MailSetting:Email"];
            var _epass = _config["EmailPass"];
            var _dispName = "Note Management Service";

            MailMessage message = new MailMessage();
            message.From = new MailAddress(_email!, _dispName);            
            message.To.Add(email);
            message.Subject = subject;
            message.Body = body;

            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.EnableSsl = true;
                smtp.Host = _config["MailSetting:Host"]!;
                smtp.Port = int.Parse(_config["MailSetting:Port"]!);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_email, _epass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                await smtp.SendMailAsync(message);
            }
            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    public async Task<bool> SendGMailAsync(List<string> emails, string subject, string body)
    {
        try
        {
            var _email = _config["MailSetting:Email"];
            var _epass = _config["EmailPass"];
            var _dispName = "Note Management Service";

            MailMessage message = new MailMessage();
            message.From = new MailAddress(_email!, _dispName);
            foreach (var email in emails)
            {
                message.To.Add(email); 
            }
            message.Subject = subject;
            message.Body = body;

            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.EnableSsl = true;
                smtp.Host = _config["MailSetting:Host"]!;
                smtp.Port = int.Parse(_config["MailSetting:Port"]!);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(_email, _epass);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.SendCompleted += (s, e) => { smtp.Dispose(); };
                await smtp.SendMailAsync(message);
            }
            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
