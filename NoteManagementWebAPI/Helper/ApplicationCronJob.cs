﻿using NoteManagementWebAPI.Model;
using static Org.BouncyCastle.Math.EC.ECCurve;

namespace NoteManagementWebAPI.Helper;

public class ApplicationCronJob    
{
    private readonly IConfiguration _config;

    public ApplicationCronJob(IConfiguration config)
    {
        _config = config;
    }

    public async Task SendEmailEveryDay()
    {
        var mailUtil = new MailUtils(_config);
        var list = new List<string>
            {
                "v.trclam@gmail.com",
                "lavo062@gmail.com"
            };
        var mail = new MailModel()
        {
            Subject = "Note Management Service - Daily mail",
            Body = "Xin chào, chúc bạn một ngày tốt lành."
        };
        await mailUtil.SendGMailAsync(list, mail.Subject, mail.Body);
    }
}
