﻿using NoteManagementWebAPI.Model;
using OfficeOpenXml;

namespace NoteManagementWebAPI.Helper;

public class FileUtils
{
    
    public static string Upload(IFormFile files, IWebHostEnvironment environment)
    {
        var filePath = environment.WebRootPath + "\\UploadedImage\\";
        try
        {
            if (files.Length > 0)
            {
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                using (FileStream fileStream = System.IO.File.Create(filePath + files.FileName))
                {
                    files.CopyTo(fileStream); // copy contents of uploaded file to targer stream
                    fileStream.Flush(); // clear buffers for this stream
                    return filePath + files.FileName;
                }
            }
            else
            {
                return "Upload failed";
            }
        }
        catch (Exception ex)
        {
            return ex.Message.ToString();
        }
    }
    
    //public static Stream CreateExcelFile (IEnumerable<NoteVM> list, Stream stream = null)
    //{
    //    using (var excelPackage = new ExcelPackage(stream ?? new MemoryStream()))
    //    {
    //        // Thêm các thông tin cho file excel
    //        excelPackage.Workbook.Properties.Author = "Admin";
    //        excelPackage.Workbook.Properties.Title = "NotesInfo";
    //        // Tạo worksheet mới để làm việc
    //        excelPackage.Workbook.Worksheets.Add("Sheet 1");
    //        // lấy worksheet vừa tjao ra để dùng
    //        var sheet = excelPackage.Workbook.Worksheets[1];
    //        // đổ dữ liệu vào file, từ dòng 1 cột 1; có thể thêm các paras để trang trí cho file excel
    //        sheet.Cells[1,1].LoadFromCollection(list);

    //        excelPackage.Save();
    //        return excelPackage.Stream;
    //    }
    //}

    //public static FileStream CreateExcelFile()
    //{
    //    using (var workbook = new XLWorkbook())
    //    {
    //        var worksheet = workbook.Worksheets.Add("Users");
    //        var currentRow = 1;
    //        worksheet.Cell(currentRow, 1).Value = "Id";
    //        worksheet.Cell(currentRow, 2).Value = "Username";
    //        foreach (var user in users)
    //        {
    //            currentRow++;
    //            worksheet.Cell(currentRow, 1).Value = user.Id;
    //            worksheet.Cell(currentRow, 2).Value = user.Username;
    //        }

    //        using (var stream = new MemoryStream())
    //        {
    //            workbook.SaveAs(stream);
    //            var content = stream.ToArray();

    //            return File(
    //                content,
    //                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    //                "users.xlsx");
    //        }
    //    }
    //}
}
