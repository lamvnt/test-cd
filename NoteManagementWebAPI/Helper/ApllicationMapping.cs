﻿using AutoMapper;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Model;

namespace NoteManagementWebAPI.Helper;

public class ApllicationMapping : Profile
{
    public ApllicationMapping()
    {
        //CreateMap<User, UserModel>().ReverseMap();
        CreateMap<Note, NoteModel>().ReverseMap();
        CreateMap<Note, NoteVM>().ReverseMap();
        CreateMap<User, UserModel>().ReverseMap();
    }
}
