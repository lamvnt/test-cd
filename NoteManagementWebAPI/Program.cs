using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Helper;
using NoteManagementWebAPI.Repository;
using Serilog;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Dang ki serrilog
var logger = new LoggerConfiguration()
  .ReadFrom.Configuration(builder.Configuration)
  .Enrich.FromLogContext()
  .CreateLogger();
builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

// Dang ki hangfire de thuc hien cron job
builder.Services.AddHangfire(config => config
        .UseSimpleAssemblyNameTypeSerializer()
        .UseRecommendedSerializerSettings()
        .UseInMemoryStorage());
builder.Services.AddHangfireServer();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// add DbContext
builder.Services.AddDbContext<NoteManagementDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("NoteManagementDB"));
});

// Dang ki auto mapper
builder.Services.AddAutoMapper(typeof(Program));

// Dang ki dependecy injection giua IRepositpry voi Repository
builder.Services.AddScoped<IAccountsRepository, AccountsRepository>();
builder.Services.AddScoped<INotesRepository, NotesRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();

// Them CORS cho tat ca moi nguoi deu xai duoc apis
builder.Services.AddCors(options
    => options.AddDefaultPolicy(policy
        => policy.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod()));

// Ma hoa chuoi token
var serectKey = builder.Configuration["JWT:SecretKey"];
var serectKeyByte = Encoding.UTF8.GetBytes(serectKey);
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(option =>
    {
        option.SaveToken = true;
        option.RequireHttpsMetadata = false;
        option.TokenValidationParameters = new TokenValidationParameters
        {
            // Tu cap token nen ko co ten & url nha cung cap token
            ValidateIssuer = false,
            ValidateAudience = false,

            //ValidAudience = builder.Configuration["JWT:ValidAudience"],  // ten nha cung cao token (neu co)
            //ValidIssuer = builder.Configuration["JWT:ValidIssuer"], // url den trang nha cung cap token (neu co)
            
            // Ki vao token (sign)
            ValidateIssuerSigningKey = true,
            //Dung thuat toan ma hoa doi xung de ma hoa chuoi token
            IssuerSigningKey = new SymmetricSecurityKey(serectKeyByte),

            ClockSkew = TimeSpan.Zero // Cung cap expired time cua token
        };
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

// Bat Cors
app.UseCors();

// Bat authenciation cho ung dung
app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

// hangfire host dashboard at "/"
app.MapHangfireDashboard("");

// call hangfire
await app.StartAsync();
RecurringJob.AddOrUpdate<ApplicationCronJob>(util => util.SendEmailEveryDay(), 
    "0 0 8 ? * *", TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time"));
await app.WaitForShutdownAsync();

app.Run();
