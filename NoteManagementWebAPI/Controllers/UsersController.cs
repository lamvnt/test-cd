﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Helper;
using NoteManagementWebAPI.Model;
using NoteManagementWebAPI.Models;
using NoteManagementWebAPI.Repository;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace NoteManagementWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IAccountsRepository _accountRepo;
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _config;

        public UsersController(IAccountsRepository accountRepo, IWebHostEnvironment environment, IConfiguration config)
        {
            _accountRepo = accountRepo;
            _environment = environment;
            _config = config;
        }
        // POST: api/Users/Login
        [HttpPost("Login")]
        public IActionResult Login(LoginModel model)
        {
            var response = _accountRepo.Login(model);
            return Ok(response);
        }

        // POST: api/Users/Register
        [HttpPost("Register")]
        public IActionResult Register(RegisterModel model)
        {
            if (model.Password != model.PasswordConfirm)
            {
                return Ok(new ApiResponse
                {
                    Success = false,
                    Message = "Password confirm does not match Password."
                });                
            }
            var response = _accountRepo.Register(model);
            return Ok(response);            
        }


        // POST: api/User/UploadAvatar
        [Authorize]
        [HttpPost("UploadAvatar")]
        public IActionResult UploadAvatar(IFormFile fileUpload)
        {
            try
            {
                // get current loggin userId
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claims = identity!.Claims.ToList();
                var userId = claims[0].Value;
                // upload file to server
                var avatarUrl = FileUtils.Upload(fileUpload, _environment);
                // save avatarUrl to db
                _accountRepo.UpdateAvatarUrl(int.Parse(userId), avatarUrl);
                return Ok(avatarUrl);

            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> SendMail()
        {
            var mailUtil = new MailUtils(_config);
            var list = new List<string>
            {
                "v.trclam@gmail.com",
                "lavo062@gmail.com"
            };
            var result = await mailUtil.SendGMailAsync(list, "Test mail", "Hiiiii");
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}
