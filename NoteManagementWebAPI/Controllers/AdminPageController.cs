﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NoteManagementWebAPI.Helper;
using NoteManagementWebAPI.Repository;
using OfficeOpenXml;
using OfficeOpenXml.Table;

namespace NoteManagementWebAPI.Controllers
{
    //[Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminPageController : ControllerBase
    {
        private readonly IUserRepository _userRepo;
        private readonly INotesRepository _noteRepo;

        public AdminPageController(IUserRepository userRepo, INotesRepository noteRepo)
        {
            _userRepo = userRepo;
            _noteRepo = noteRepo;
        }
        
        // GET: api/AdminPage/GetUsers        
        [HttpGet("GetUsers")]
        public IActionResult GetUsers()
        {
            var userList = _userRepo.GetAll();
            return Ok(userList);
        }

        // PUT: api/AdminPage/AssignAdmin/{userId}
        [HttpPut("AssignAdmin/{userId}")]
        public IActionResult AssignAdmin(int userId)
        {
            try
            {
                _userRepo.AssignAdmin(userId);
                return NoContent();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }
        
        // GET: api/AdminPage/GetNotes/{userId}
        [HttpGet("GetNotes")]
        public IActionResult GetNotes()
        {
            try
            {
                var notes = _noteRepo.GetAll();
                return Ok(notes);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        // GET: api/AdminPage/GetNotes/{userId}
        [HttpGet("GetNotes/{userId}")]
        public IActionResult GetNotesByUser(int userId)
        {
            try
            {
                var notes = _noteRepo.GetNotesByUser(userId);
                return Ok(notes);

            } catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        // PUT: api/AdminPage/DisableAccount/{userId}
        [HttpPut("DisableAccount/{userId}")]
        public IActionResult DisableAccount(int userId)
        {
            try
            {
                _userRepo.DisableUser(userId);
                return NoContent();

            } catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/AdminPage
        [HttpGet("ExportExcel")]
        public async Task<IActionResult> ExportExcel(CancellationToken cancellationToken)
        {
            // query data from database  
            await Task.Yield();
            var dataList = _noteRepo.GetTopCurrentNote(10);

            var stream = new MemoryStream();

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(dataList, true);
                package.Save();
            }
            stream.Position = 0;
            string excelName = $"NotesInfo-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            //return File(stream, "application/octet-stream", excelName);  
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }
    }
}
