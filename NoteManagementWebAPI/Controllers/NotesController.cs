﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.EntityFrameworkCore;
using NoteManagementWebAPI.Data;
using NoteManagementWebAPI.Model;
using NoteManagementWebAPI.Repository;

namespace NoteManagementWebAPI.Controllers
{    
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INotesRepository _noteRepo;

        public NotesController(INotesRepository noteRepo)
        {
            _noteRepo = noteRepo;
        }

        // GET: api/Notes
        [Authorize]
        [HttpGet]
        public IActionResult GetNotes() // chỉ xem được những note mà bản thân đã tạo
        {
            try
            {
                // get current loggin userID
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claims = identity!.Claims.ToList();
                var userId = claims[0].Value;

                var notes = _noteRepo.GetNotesByUser(int.Parse(userId));
                return Ok(notes);

            } catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Notes/5
        [Authorize]
        [HttpGet("{id}")]
        public IActionResult GetNote(Guid id)
        {
            var note = _noteRepo.GetById(id);

            if (note == null)
            {
                return NotFound();
            }

            return Ok(note);
        }

        // PUT: api/Notes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [Authorize(Roles = "ActiveUser")]
        [HttpPut("Edit/{id}")]
        public IActionResult EditNote(Guid id, NoteModel model)
        {
            try
            {
                _noteRepo.Update(id, model);
                return Ok();
            } catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Notes/Create
        [Authorize(Roles = "ActiveUser")]
        [HttpPost("Create")]
        public IActionResult CreateNote(NoteModel model)
        {
            try
            {
                // get current loggin userID
                var identity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claims = identity!.Claims.ToList();
                var userId = claims[0].Value;

                var noteId = _noteRepo.Create(model, int.Parse(userId));
                var note = _noteRepo.GetById(noteId);
                return Created("api/Notes/CreateNote", note);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);           
            }            
        }

        // DELETE: api/Notes/5
        [Authorize(Roles = "ActiveUser")]
        [HttpDelete("{id}")]
        public IActionResult DeleteNote(Guid id)
        {
            try
            {
                _noteRepo.Delete(id);
                return NoContent();
            } catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }        
    }
}
