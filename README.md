# Rest API cho hệ thống quản lí notes
viết REST API cho hệ thống quản lý note với yêu cầu như sau. 
1. [x] USER: đăng ký tài khoản (1 điểm) 
2. [x] USER: đăng nhập, mỗi lần user đăng nhập thì sẽ được lưu lại lịch sử đăng nhập. (1 điểm) 
3. [x] USER: thêm xóa sửa các note của mình(ngày tạo, ngày sửa, nội dung note, tạo bởi ai) (1 điểm)


4. [x] ADMIN: xem danh sách user trong hệ thống(username, lần cuối đăng nhập, số noted đã lưu, ngày tạo account, avatarURL) (1 điểm) 
5. [x] ADMIN: gán quyền admin cho user được trở thành admin.(1 điểm)
6. [x] ADMIN: xem tất cả các noted theo user . (1 điểm) 
7. [x] ADMIN: khóa tài khoản user không cho phép thêm note(nhưng vẫn có thể đăng nhập và thay đổi avatar được) (1 điểm) 
8. [x] ADMIN: xuất ra file excel 10 noted được tạo mới nhất(username, noted, ngày tạo note) (1 điểm) 

9. [ ] tự động gửi email vào lúc 8h sáng(giờ việt nam) cho toàn bộ user trong hệ thống với nội dung "xin chào chúc bạn ngày mới vui vẻ". (1 điểm) 
10. [x] user upload file ảnh avatar (1 điểm) 
11. [ ] bonus: viết unit test > 50% line coverrage + 1 điểm - yêu cầu: sử dụng database để lưu trữ dữ liệu 
- gợi ý: 
	- sử dụng json web token để authenticate & authorization 
	- sử dụng hangfire để chạy cron job.

## References
- [Use Postman for API testing - youtube](https://youtu.be/CmdheJaN7MI)
- [Athenticatetion with JWT - youtube](https://youtu.be/AQwS4-5YV4o)
- [Authentication with identity - youtube](https://youtu.be/9YSOZgBvWXY)
- [Demo JWT - how to read data from JWT - youtube](https://youtu.be/EuPGklU5lKY)
- [Roles base authorization - youtube](https://youtu.be/ILi29Ikn6jY)
- [Write text log with serrilog - youtube](https://youtu.be/MYKTwvowMUI)
- [Xuất file excel với essplus- toidicodedao](https://toidicodedao.com/2015/11/24/series-c-hay-ho-epplus-thu-vien-excel-ba-dao-phan-1/)
- [Export data to excel with asp net core](https://www.c-sharpcorner.com/article/using-epplus-to-import-and-export-data-in-asp-net-core/)
- [EPPlus license](https://epplussoftware.com/developers/licenseexception)
- [Gửi email](https://xuanthulab.net/asp-net-core-gui-mail-trong-ung-dung-web-asp-net.html)
- [Gửi email & hangfire](https://www.twilio.com/blog/recurring-emails-csharp-dotnet-with-hangfire-sendgrid)
- [cron expression](https://en.wikipedia.org/wiki/Cron#CRON_expression)
- [generate cron expression](https://www.freeformatter.com/cron-expression-generator-quartz.html#tabs-4)

</br>&copy; 2023 **Võ Ngọc Trúc Lam**